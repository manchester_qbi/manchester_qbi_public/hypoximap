'''
'''

import numpy as np
from scipy.stats import ttest_1samp

SIGLEVEL = 0.05

hypoxia_status = (
    'No enhancement',
    'OE-only enhanced',
    'DCE-only enhanced',
    'OE + DCE enhanced'
)

hypoxia_interpretation = (
    'Necrotic',
    'Diffused oxygen',
    'Hypoxic',
    'Normoxic'
)

def hypoxistat(
        gd_signals, 
        gd_limits, 
        oe_signals, 
        oe_limits, mask = None,
        driftCorrectionMethod = 'None', 
        drift_mask = None, 
        siglevel = SIGLEVEL):
    '''
    HYPOXISTAT Returns the hypoxia status of DCE and OE timecourses.
  
   hstat = hypoxistat(gdsigvol, gd_limits, oesigvol, oe_limits)
   returns the hypoxia statuses as a volume with the same spatial 
   dimensions as the input volumes gdsigvol and oesigvol.  
 
   Where both Gd and OE signals increase, the status is '4' (normoxic).  
   Where Gd increases, but OE does not, the status is '3' (hypoxic).  
   Where Gd does not increase, an increase in OE indicates diffused oxygen
   (status 2).  Where there is no increase, the tissue is not perfused or 
   supplied with oxygen, and has status 1 ('necrotic').  Typically, 1 and
   2 are combined into a non-perfused region.  

   gd_limits is a 1x4 element vector containing the temporal positions of
   gdsigvol to use for the baseline start, baseline end,
   enhancement start, enhancement end.  oe_limits is a 1x4 element vector
   containing the temporal positions of oesigvol to use for the
   baseline start, baseline end, enhancement start, enhancement end.  A
   one-sided t-test with significance level 0.05 is applied to the mean
   of the baseine and enhancement values to determine significant
   enhancement.  

   hstat = hypoxistat(gdsigvol, gd_limits, oesigvol, oe_limits, mask) 
   limits the analysis to the nonzero elements of mask.

   hstat = hypoxistat(gdsigvol, gd_limits, oesigvol, oe_limits, mask, driftCorrectionMethod)
   By default, no drift correction is applied.  driftCorrectionMethod may
   be 'none', 'byvoxel' or 'allvoxels'.  If drift correction is applied,
   the baseline signals (for eachvoxel) or the mean of the baseline
   signals (for allvoxels) will be rotated so that the signal follows a
   straight line along the baseline.  The rotated enhancement signals will
   then be compared to the baseline to find the hypoxia status of each
   voxel.

   hstat = hypoxistat(gdsigvol, gd_limits, oesigvol, oe_limits, mask, driftCorrectionMethod, siglevel)
   By default, a significance level siglevel=0.05 will be applied for the t-test
   comparing the change in the signal to the baseline.

   [hypoxia_status, hypoxia_status, gd_corrected, oe_corrected] ...
       = hypoxistat(...)
   returns the drift corrected Gd and OE signals as volumes.

   [hypoxiastatusvol, hypoxia_status, gd_corrected, oe_corrected, ...
       gdsigmatout, oesigmatout] = hypoxistat(...)
   returns matrices of hypoxia status, and Gd and OE signals (drift
   corrected if specified) for the non-zero elements of mask.

   ... = hypoxistat(gd_signals, gd_limits, oe_signals, oe_limits, ...) accepts
   input timecourses as matrices with each row of the matrix being a
   timecourse.  Outputs are returned as corresponding matrices or vectors.

   Ross A Little 22 June 2021
   (c) 2014 - 2021 The University of Manchester 
   For non-clinical use only. All rights reserved.

   Please cite reference Radiology (2018) Sep288(3):739-747. doi: 10.1148/radiol.2018171531

   See also: HYPOXIMAPSHOW

    Created RAL 19/5/14
    28/3/17 allow switching between linear drift correction using average or single voxel
    27/5/21 create a function wrapper
    30/6/21 added optional significance level parameter

    05/10/2022 translated to python by Mike Berks
 '''
    # This function will operate on either input volumes of signals (4D) or
    # matrices (2D).
    if gd_signals.ndim == 4:
        return hypoxiastat4D(
            gd_signals = gd_signals, 
            gd_limits = gd_limits, 
            oe_signals = oe_signals, 
            oe_limits = oe_limits, 
            mask = mask, 
            driftCorrectionMethod = driftCorrectionMethod, 
            drift_mask = drift_mask, 
            siglevel = siglevel)

    elif gd_signals.ndim == 2:
        return hypoxiastat2D(
            gd_signals = gd_signals, 
            gd_limits = gd_limits, 
            oe_signals = oe_signals, 
            oe_limits = oe_limits, 
            driftCorrectionMethod = driftCorrectionMethod, 
            siglevel = siglevel)

    else:
        raise ValueError('Input signals must be 2D or 4D')

def hypoxiastat2D(
    gd_signals, gd_limits, oe_signals, oe_limits, 
    driftCorrectionMethod = 'None', siglevel = SIGLEVEL,
    gd_mean_signals = None, oe_mean_signals = None):

    #Drift correction should either be a string (used for both OE + DCE) or a 
    # 2 element list/tuple/container specifying OE/DCE drift corrections separately
    if type(driftCorrectionMethod) is str:
        driftCorrectionMethod = (driftCorrectionMethod,driftCorrectionMethod)
    elif len(driftCorrectionMethod) == 1:
        driftCorrectionMethod = (driftCorrectionMethod[0],driftCorrectionMethod[0])

    # Check signal matrices have same number of signals (perfusion and oxygen
    # for each voxel)
    assert gd_signals.ndim == 2 and oe_signals.ndim == 2 and \
        gd_signals.shape[0] == oe_signals.shape[0], \
        'gd_signals and oe_signals should be matrices with same number of rows.'

    # Are input arguments valid?
    def check_valid_limits(limits, nt, varname1, varname2):
        try:
            limits = np.array(limits)
        except:
            raise ValueError(f'Incorrect type: {varname1} could not be converted to np.array')

        assert limits.size == 4 and limits.dtype == int and \
            np.all(limits[:-1] < limits[1:]) and \
            limits[0] >= 1 and limits[-1] <= nt, \
            f'{varname1} should be a 1x4 vector of indices to the columns of '\
            f' {varname2} sorted in ascending order.'

        #Limits are given indexed from 1 and assumed to be end inclusive. Convert them
        #here to be indexed from 0 and end exclusive for np.array slicing by subtracting 1
        #from the first and 3rd elements
        limits[0] -= 1
        limits[2] -= 1
        return limits
    
    assert np.isscalar(siglevel) and (0 < siglevel < 1),\
        'siglevel must be a scalar between 0 and 1.'
    
    # Check limits vectors
    gd_limits = check_valid_limits(
        gd_limits, gd_signals.shape[1], 'gd_limits', 'gd_signals')
    oe_limits = check_valid_limits(
        oe_limits, oe_signals.shape[1], 'oe_limits', 'oe_signals')

    # Perform drift correction
    gd_corrected = drift_correct(
        gd_signals, gd_limits, driftCorrectionMethod[0], gd_mean_signals)
    oe_corrected = drift_correct(
        oe_signals, oe_limits, driftCorrectionMethod[1], oe_mean_signals)
    
    # Test enhancment
    oe_enhancement = test_enhancement(oe_corrected, oe_limits, siglevel)
    gd_enhancement = test_enhancement(gd_corrected, gd_limits, siglevel)

    # set hypoxia status to NaNs
    hypoxia_status = np.empty(oe_signals.shape[0], dtype = int)

    # set voxels with oe and gd rise to 4               normoxic
    status_jointrise = oe_enhancement & gd_enhancement
    hypoxia_status[status_jointrise] = 4

    # set voxels with gd rise only to 3                 hypoxic
    status_gd_rise_only = gd_enhancement & ~oe_enhancement
    hypoxia_status[status_gd_rise_only] = 3

    # set voxels with oe rise only to 2                 diffused oxygen
    status_oe_rise_only = oe_enhancement & ~gd_enhancement
    hypoxia_status[status_oe_rise_only] = 2

    # set voxels with no enhancement at all to 1        necrotic
    status_no_enhancement = ~oe_enhancement & ~gd_enhancement
    hypoxia_status[status_no_enhancement] = 1

    return hypoxia_status, gd_corrected, oe_corrected

def hypoxiastat4D(gd_signals, gd_limits, oe_signals, oe_limits, 
    mask = None, driftCorrectionMethod = 'None', drift_mask = None, 
    siglevel = SIGLEVEL):
      
    # Check mask, OE and Gd volumes are valid - are the they same spatial
    # dimensions? are they correct dimensionality?
    assert gd_signals.ndim ==4 and oe_signals.ndim == 4,\
        'Input signals must be either in matrix or 4D volume form'

    # What are the spatial sizes of the input signals?
    spatsz = oe_signals.shape[:-1]
    
    # Does mask exist? If not, set to whole volume
    if mask is None:
        mask = np.full(spatsz, True)

    #Parse the drift mask
    drift_mask = parse_drift_mask(drift_mask, mask)
    
    assert spatsz == gd_signals.shape[:-1],'Check OE and Gd spatial sizes are the same.'
    assert mask.shape == spatsz,'Check mask and input signal spatial sizes are the same.'
    assert drift_mask[0].shape == spatsz,'Check drift mask and input signal spatial sizes are the same.'
    assert drift_mask[1].shape == spatsz,'Check drift mask and input signal spatial sizes are the same.'
    
    #Get average OE and DCE signals
    gd_mean_signals = np.mean(
        gd_signals[:,:,:,gd_limits[0]-1:gd_limits[1]][drift_mask[0]], axis = 0)
    oe_mean_signals = np.mean(
        oe_signals[:,:,:,oe_limits[0]-1:oe_limits[1]][drift_mask[1]], axis = 0)
    
    #Call hypoxiastat2D on the input signals flattened by the mask
    hypoxia_status2D, gd_corrected2D, oe_corrected2D = hypoxiastat2D(
        gd_signals[mask], gd_limits, oe_signals[mask], oe_limits, 
        driftCorrectionMethod, siglevel, gd_mean_signals, oe_mean_signals)

    #Reshape the outputs
    hypoxia_status = np.full(spatsz, np.nan)
    hypoxia_status[mask] = hypoxia_status2D
    
    # and gd signals
    gd_corrected = np.full(gd_signals.shape, np.nan)
    gd_corrected[mask] = gd_corrected2D
    
    # and oe signals
    oe_corrected = np.full(oe_signals.shape, np.nan)
    oe_corrected[mask] = oe_corrected2D
        
    return hypoxia_status, gd_corrected, oe_corrected

def parse_drift_mask(drift_mask, mask):
    try:
        #Will run without failure if drift_mask is a str or container that implements len
        if type(drift_mask) is str:
            drift_mask = [drift_mask,drift_mask]
        elif len(drift_mask) != 2:
            drift_mask = [drift_mask[0],drift_mask[0]]
    except:
        #drift_mask doesn't have len
        drift_mask = [drift_mask,drift_mask]

    for i in range(2):    
        if drift_mask[i] is None:
            drift_mask[i] = mask
        elif type(drift_mask[i]) is str and drift_mask[i].lower() == 'all':
            drift_mask[i] = np.full_like(mask, True)
    
    return drift_mask

def drift_correct(signals_full, limits, method, mean_signals = None):
    if method is None or method.lower() == 'none': #Do nothing
        return signals_full

    #For all voxels and by voxel, we'll need x-vectors set-up for
    #the timepoints we're fitting
    x_baseline = np.arange(limits[0], limits[1])
    x_full = np.arange(signals_full.shape[1])
    signals_baseline = signals_full[:,limits[0]:limits[1]]

    if method.lower() == 'allvoxels':
        #For all voxels fit a single straight-line to the mean baseline
        #time-series of all voxels, then subtract the projection
        # of this line from the full time-series of all signals
        if mean_signals is None:
            mean_signals = np.mean(signals_baseline, axis = 0)
        P = np.polyfit(x_baseline, mean_signals, 1)
        corrected_signals = signals_full - P[0]*x_full
            

    elif method.lower() == 'byvoxel':
        #For by voxel, fit a straight line to the baseline time-series
        #of each voxel, then subtract the projection
        # of this line from the full time-series
        corrected_signals = np.empty_like(signals_full)
        for voxel in range(signals_baseline.shape[0]):
            y_baseline = signals_baseline[voxel,:]
            P = np.polyfit(x_baseline, y_baseline, 1)
            
            corrected_signals[voxel,:] = signals_full[voxel,:] - P[0]*x_full
                
    else:
        raise ValueError(
            "driftCorrectionMethod should be 'None', 'ByVoxel' or 'AllVoxels'.")

    return corrected_signals

def test_enhancement(signals, limits, siglevel, method = 'ttest'):
    # Is there enhancement?
    baseline = np.mean(signals[:,limits[0]:limits[1]], axis=1, keepdims=True)
    curve =    signals[:,limits[2]:limits[3]]

    if method == 'ttest':
        # if no change in signal, set to no-enhancement - note any signals with NaNs in
        # will have p-val NaN (due to ttest_ind propogate policy), which will evaluate to
        # false as per the Matlab implementation
        _,p_vals = ttest_1samp(curve, baseline, 
            axis = 1, alternative = 'greater', nan_policy='propagate')
        enhancement = p_vals <= siglevel

    elif method == 'strict':
        # this provides for a strict comparison of whether enhancement is > 0:
        enhancement = ( np.mean(curve,axis=1) > np.mean(baseline,axis=1) )

    else:
        raise ValueError('method should be ttest or strict')
    
    return enhancement

