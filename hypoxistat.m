function [hypoxiastatusvol, hypoxiastatusmat, ...
    gdsigvolout, oesigvolout, gdsigmatout, oesigmatout] ...
    = hypoxistat(gdsigin, gdlimits, oesigin, oelimits, mask, driftCorrectionMethod, siglevel)
%HYPOXISTAT Returns the hypoxia status of DCE and OE timecourses.
%  
%   hstat = hypoxistat(gdsigvol, gdlimits, oesigvol, oelimits)
%   returns the hypoxia statuses as a volume with the same spatial 
%   dimensions as the input volumes gdsigvol and oesigvol.  
% 
%   Where both Gd and OE signals increase, the status is '4' (normoxic).  
%   Where Gd increases, but OE does not, the status is '3' (hypoxic).  
%   Where Gd does not increase, an increase in OE indicates diffused oxygen
%   (status 2).  Where there is no increase, the tissue is not perfused or 
%   supplied with oxygen, and has status 1 ('necrotic').  Typically, 1 and
%   2 are combined into a non-perfused region.  
%
%   gdlimits is a 1x4 element vector containing the temporal positions of
%   gdsigvol to use for the baseline start, baseline end,
%   enhancement start, enhancement end.  oelimits is a 1x4 element vector
%   containing the temporal positions of oesigvol to use for the
%   baseline start, baseline end, enhancement start, enhancement end.  A
%   one-sided t-test with significance level 0.05 is applied to the mean
%   of the baseine and enhancement values to determine significant
%   enhancement.  
%
%   hstat = hypoxistat(gdsigvol, gdlimits, oesigvol, oelimits, mask) 
%   limits the analysis to the nonzero elements of mask.
%
%   hstat = hypoxistat(gdsigvol, gdlimits, oesigvol, oelimits, mask, driftCorrectionMethod)
%   By default, no drift correction is applied.  driftCorrectionMethod may
%   be 'none', 'byvoxel' or 'allvoxels'.  If drift correction is applied,
%   the baseline signals (for eachvoxel) or the mean of the baseline
%   signals (for allvoxels) will be rotated so that the signal follows a
%   straight line along the baseline.  The rotated enhancement signals will
%   then be compared to the baseline to find the hypoxia status of each
%   voxel.
%
%   hstat = hypoxistat(gdsigvol, gdlimits, oesigvol, oelimits, mask, driftCorrectionMethod, siglevel)
%   By default, a significance level siglevel=0.05 will be applied for the t-test
%   comparing the change in the signal to the baseline.
%
%   [hypoxiastatusmat, hypoxiastatusmat, gdsigvolout, oesigvolout] ...
%       = hypoxistat(...)
%   returns the drift corrected Gd and OE signals as volumes.
%
%   [hypoxiastatusvol, hypoxiastatusmat, gdsigvolout, oesigvolout, ...
%       gdsigmatout, oesigmatout] = hypoxistat(...)
%   returns matrices of hypoxia status, and Gd and OE signals (drift
%   corrected if specified) for the non-zero elements of mask.
%
%   ... = hypoxistat(gdsigmat, gdlimits, oesigmat, oelimits, ...) accepts
%   input timecourses as matrices with each row of the matrix being a
%   timecourse.  Outputs are returned as corresponding matrices or vectors.
%
%   Ross A Little 22 June 2021
%   (c) 2014 - 2021 The University of Manchester 
%   For non-clinical use only. All rights reserved.
%
%   Please cite reference Radiology (2018) Sep;288(3):739-747. doi: 10.1148/radiol.2018171531
%
%   See also: HYPOXIMAPSHOW

% Created RAL 19/5/14
% 28/3/17 allow switching between linear drift correction using average or single voxel
% 27/5/21 create a function wrapper
% 30/6/21 added optional significance level parameter

% This function will operate on either input volumes of signals (4D) or
% matrices (2D).  
assert( (ismatrix(gdsigin) && ismatrix(oesigin)) || (ndims(gdsigin)==4 && ndims(oesigin)==4), ...
    'Input signals must be either in matrix or 4D volume form');
    
% Is driftCorrectionMethod specified?
if ~exist('driftCorrectionMethod','var')
    driftCorrectionMethod = 'none';
end

% is significance level for t-test specified and valid?
if ~exist('siglevel','var')
    siglevel = 0.05;
end
assert(isscalar(siglevel) && siglevel>0 && siglevel<1, 'siglevel must be a scalar between 0 and 1.')

% Set VOLUMEFLAG to reflect if input sigs are volumes or matrices.
if ismatrix(gdsigin), VOLUMEFLAG = false; else VOLUMEFLAG = true; end

% PART ONE: If we have volumes then we need to convert to matrix form:

if VOLUMEFLAG
    % convert volumes to matrix form

    % What are the spatial sizes of the input signals?
    spatszoe = [size(oesigin,1), size(oesigin,2), size(oesigin,3)];
    spatszgd = [size(gdsigin,1), size(gdsigin,2), size(gdsigin,3)];
    
    % Does mask exist? If not, set to whole volume
    if ~exist('mask','var') || isempty(mask)
        mask = ones(spatszgd);
    end
    
    % Check mask, OE and Gd volumes are valid - are the they same spatial
    % dimensions? are they correct dimensionality?
    assert(isequal(spatszoe,spatszgd),'Check OE and Gd spatial sizes are the same.');
    assert(isequal(size(mask), spatszoe),'Check mask and input signal spatial sizes are the same.');
    assert(ndims(oesigin)==4,'OE signal volume should have four dimensions.');
    assert(ndims(gdsigin)==4,'Gd signal volume should have four dimensions.');
    assert(ndims(mask)==3,'Mask volume should have three dimensions.');
    
    % Inputs seem valid.  Now extract signal courses and pass to
    % hypoxiastatusfrommatrices().
    
    % flatten the input signals to be spatial position (rows) by time (cols)
    gdsigvol_flat = reshape(gdsigin,prod(spatszgd),size(gdsigin,4));
    oesigvol_flat = reshape(oesigin,prod(spatszoe),size(oesigin,4));
    
    % restrict the elements to mask elements
    maskelements = find(mask);
    gdsigmat = gdsigvol_flat(maskelements,:);
    oesigmat = oesigvol_flat(maskelements,:);
    
else
    
    % we are in matrix form! 
    gdsigmat = gdsigin;
    oesigmat = oesigin;
    
end

% PART TWO: Analyse the signals as matrices:

[hypoxiastatusmat, gdsigmatout, oesigmatout] = ...
    hypoxiastatusfrommatrices(gdsigmat, gdlimits, oesigmat, oelimits, driftCorrectionMethod, siglevel);

% PART THREE: If we had volumes as input, we need to return volumes:

if VOLUMEFLAG
    
    % create volume of hypoxia status
    hypoxiastatusvol = nan(size(mask));
    hypoxiastatusvol(maskelements) = hypoxiastatusmat(:);
    
    % and gd signals
    gdsigvolout_flat = nan(prod(spatszgd),size(gdsigin,4));
    gdsigvolout_flat(maskelements,:) = gdsigmat;
    gdsigvolout = reshape(gdsigvolout_flat, size(gdsigin));
    
    % and oe signals
    oesigvolout_flat = nan(prod(spatszoe),size(oesigin,4));
    oesigvolout_flat(maskelements,:) = oesigmat;
    oesigvolout = reshape(oesigvolout_flat, size(oesigin));
    
else
    
    % otherwise we need to output the matrices
    hypoxiastatusvol = hypoxiastatusmat;
    gdsigvolout = gdsigmatout;
    oesigvolout = oesigmatout;

end

end

% The code for Part Two is here, using matrices of input signals:

function [hypoxiastatusmat, gdsigmatout, oesigmatout] = ...
    hypoxiastatusfrommatrices(gdsigmat, gdlimits, oesigmat, oelimits, driftCorrectionMethod, siglevel)

% Are input arguments valid?

% Check signal matrices have same number of signals (perfusion and oxygen
% for each voxel)
assert(ismatrix(gdsigmat),'gdsigmat and oesigmat should be matrices with same number of rows.')
assert(ismatrix(oesigmat),'gdsigmat and oesigmat should be matrices with same number of rows.')
assert(size(gdsigmat,1)==size(oesigmat,1),'gdsigmat and oesigmat should be matrices with same number of rows.')

% Check limits vectors
assert(numel(gdlimits)==4,'gdlimits should be a 1x4 element vector.')
assert(numel(oelimits)==4,'oelimits should be a 1x4 element vector.')
assert(max(gdlimits)<=size(gdsigmat,2),'Check elements of gdlimits refer to columns in gdsigmat.')
assert(max(oelimits)<=size(oesigmat,2),'Check elements of oelimits refer to columns in oesigmat.')
assert(min(gdlimits)>0,'Check elements of oelimits refer to columns in oesigmat.')
assert(min(oelimits)>0,'Check elements of oelimits refer to columns in oesigmat.')
assert(isequal(sort(int16(gdlimits)),gdlimits),'Ensure gdlimits are integers in ascending numerical order.')
assert(isequal(sort(int16(oelimits)),oelimits),'Ensure oelimits are integers in ascending numerical order.')

% Check drift correction option
if ~exist('driftCorrectionMethod','var')
    driftCorrectionMethod = 'none';
end
if isempty(driftCorrectionMethod)
    driftCorrectionMethod = 'none';
end
assert(isequal(lower(driftCorrectionMethod),'none') || ...
    isequal(lower(driftCorrectionMethod),'byvoxel') || isequal(lower(driftCorrectionMethod),'allvoxels'), ...
    'driftCorrectionMethod should be ''None'',''ByVoxel'' or ''AllVoxels''.') 

% Perform drift correction
% What are the baseline signals?
oe_baseline = oesigmat(:,oelimits(1):oelimits(2));
gd_baseline = gdsigmat(:,gdlimits(1):gdlimits(2));
switch driftCorrectionMethod
    case 'allvoxels'
        % Linear drift of mean signal
        P_oe = polyfit(1:size(oe_baseline,2), mean(oe_baseline), 1);
        P_gd = polyfit(1:size(gd_baseline,2), mean(gd_baseline), 1);
        oesigmatout = oesigmat - repmat((P_oe(1).*(1:size(oesigmat,2))+P_oe(2)),size(oesigmat,1),1) + P_oe(2);
        gdsigmatout = gdsigmat - repmat((P_gd(1).*(1:size(gdsigmat,2))+P_gd(2)),size(gdsigmat,1),1) + P_gd(2);
    case 'byvoxel'
        % Linear drift voxel by voxel
        oesigmatout = nan(size(oesigmat));
        gdsigmatout = nan(size(gdsigmat));
        for voxelDriftIter = 1:size(oe_baseline,1)
            voxel_oe_baseline = oe_baseline(voxelDriftIter,:);
            P_oe = polyfit(1:size(oe_baseline,2), voxel_oe_baseline, 1);
            voxel_gd_baseline = gd_baseline(voxelDriftIter,:);
            P_gd = polyfit(1:size(gd_baseline,2), voxel_gd_baseline, 1);
            oesigmatout(voxelDriftIter,:) = oesigmat(voxelDriftIter,:) - ( P_oe(1).*(1:size(oesigmat,2))+P_oe(2) )   + P_oe(2);
            gdsigmatout(voxelDriftIter,:) = gdsigmat(voxelDriftIter,:) - ( P_gd(1).*(1:size(gdsigmat,2))+P_gd(2) )   + P_gd(2);
        end
    case 'none'
        oesigmatout = oesigmat;
        gdsigmatout = gdsigmat;
end

% Extract the corrected baseline and uptake
oe_baseline_corrected = oesigmatout(:,oelimits(1):oelimits(2));
gd_baseline_corrected = gdsigmatout(:,gdlimits(1):gdlimits(2));
oe_curve_corrected = oesigmatout(:,oelimits(3):oelimits(4));
gd_curve_corrected = gdsigmatout(:,gdlimits(3):gdlimits(4));

% Is there enhancement?

% this provides for a strict comparison of whether enhancement is > 0:
%oe_enhancement = ( mean(oe_curve_corrected,2) > mean(oe_baseline_corrected,2) );
%gd_enhancement = ( mean(gd_curve_corrected,2) > mean(gd_baseline_corrected,2) );

% this provides a t-test with significance alpha, the rate of false +ives:
oe_enhancement = ttest(oe_curve_corrected - repmat(mean(oe_baseline_corrected,2),1,size(oe_curve_corrected,2)),[],siglevel,'right',2);
gd_enhancement = ttest(gd_curve_corrected - repmat(mean(gd_baseline_corrected,2),1,size(gd_curve_corrected,2)),[],siglevel,'right',2);

% if no change in signal, set to no-enhancement
oe_enhancement(isnan(oe_enhancement)) = 0;
gd_enhancement(isnan(gd_enhancement)) = 0;

% set hypoxia status to NaNs
hypoxiastatusmat = nan(size(oesigmat,1),1);

% set voxels with oe and gd rise to 4               normoxic
status_jointrise = oe_enhancement & gd_enhancement;
hypoxiastatusmat( status_jointrise ) = 4;

% set voxels with gd rise only to 3                 hypoxic
status_gd_rise_only = gd_enhancement & ~oe_enhancement;
hypoxiastatusmat( status_gd_rise_only ) = 3;

% set voxels with oe rise only to 2                 diffused oxygen
status_oe_rise_only = oe_enhancement & ~gd_enhancement;
hypoxiastatusmat( status_oe_rise_only ) = 2;

% set voxels with no enhancement at all to 1        necrotic
status_no_enhancement = ~oe_enhancement & ~gd_enhancement;
hypoxiastatusmat( status_no_enhancement ) = 1;

end

