# Hypoximap

Hypoximap: a Matlab toolkit for detecting hypoxia using oxygen-enhanced MRI.

The QBI Lab has developed methods to acquire images sensitive to hypoxia using DCE-MRI and OE-MRI.  This code allows maps to be produced of the hypoxia status of each voxel according to the change in the signals, which hypoxic voxels being identified as perfused voxels which are refractory to oxygen enhancement.

Example usage:

`% check code running correctly!`  
`runtests tests_hypoximap`  
  
`% make sure you have some variables gd_dyn_sig_vol, oe_dyn_sig_vol with at least 75 timepoints each, and maybe make a mask...`  
`% then calculate hypoxia status of input signals`  
`hstat = hypoxistat(gd_dyn_sig_vol, [2 5 10 75], oe_dyn_sig_vol, [2 25 30 75], mask)`  
  
`% and now show the hypoxia map`  
`hypoximapshow(hstat);`

Please cite Little et al, 'Mapping Hypoxia in Renal Carcinoma with Oxygen-enhanced MRI: Comparison with Intrinsic Susceptibility MRI and Pathology' in Radiology (2018) Sep;288(3)739-747. doi: 10.1148/radiol.2018171531

By Ross A Little 22 June 2021  
(c) 2014 - 2021 The University of Manchester
