function hypoximapshow(hmap)
%HYPOXIMAPSHOW Displays a hypoxia map in the current figure
%
%   HYPOXIMAPSHOW(HMAP) plots HMAP (in a format as output from
%   hypoxiastatusfromvolumes()) as a hypoxia map.  HMAP must be 2D or 3D.
%   If HMAP is 3D then it will be displayed as a montage of slices.
%
%   Ross A Little 29 June 2021
%   (c) 2014 - 2021 The University of Manchester 
%   For non-clinical use only. All rights reserved.
%
%   Please cite reference Radiology (2018) Sep;288(3):739-747. doi: 10.1148/radiol.2018171531
%
%   See also: HYPOXIMAP

if ismatrix(hmap)
    % use imshow to display if we have a single slice
    imshow(hmap,[0 4]);
    colormap([0 0 0; 128 128 128; 128 128 128; 38 157 246; 255 193 0] ./ 255)
    colorbar off
else
    % display as a montage if we have multiple slices
    assert(ndims(hmap)==3, 'Hypoxia map should be a single slice or 3D volume.')
    numSlices = size(hmap,3);
    plotSize = ceil(sqrt(numSlices));
    for sliceNumber = 1:numSlices
        subplot(plotSize, plotSize, sliceNumber)
        imshow(hmap(:,:,sliceNumber),[0 4]);
        colormap([0 0 0; 128 128 128; 128 128 128; 38 157 246; 255 193 0] ./ 255)
        colorbar off
    end
end