% Unit tests for hypoximap methods to measure hypoxic fraction of tumours
% using oxygen and Gd enhancement.
%
% By Ross Little 26 May 2021
% (c) The University of Manchester

% Let us create some example curves to represent oxygen changes (from
% negative to positive going) and contrast enhancement changes (from zero
% to positive)

% use a sigmoid to model uptake of contrast
sigfun = @(x,minsig,uptakepoint,uptakerate,maxsig) minsig + (maxsig-minsig) ./ (1+exp(-uptakerate.*(x-uptakepoint)));

% create an OE and DCE volume 

oesize = [4 6 5 80];
dcesize = [4 6 5 75];

oevol = zeros(oesize);
oetime = 1:oesize(4);

dcevol = zeros(dcesize);
dcetime = 1:dcesize(4);

spatialsize = oesize(1:3);

% define the centre timepoint for the contrast change
oeswitch = 25;
dceswitch = 20;

% initial signals
oesigbaseline = 100;
dcesigbaseline = 200;

% how quickly should signals change?  (higher is faster)
oeswitchrate = 1;
dceswitchrate = 1;

% populate a few elements of the image volumes
currslice = 2;

% normoxic
currrow = 2; currcol = 2;
oevol(currrow,currcol,currslice,:) = sigfun(oetime,oesigbaseline,oeswitch,oeswitchrate,oesigbaseline*1.1);
dcevol(currrow,currcol,currslice,:) = sigfun(dcetime,dcesigbaseline,dceswitch,dceswitchrate,dcesigbaseline*2.5);
% normoxic
currrow = 3; currcol = 2;
oevol(currrow,currcol,currslice,:) = sigfun(oetime,oesigbaseline,oeswitch,oeswitchrate,oesigbaseline*1.05);
dcevol(currrow,currcol,currslice,:) = sigfun(dcetime,dcesigbaseline,dceswitch,dceswitchrate,dcesigbaseline*2.0);

% hypoxic
currrow = 2; currcol = 3;
oevol(currrow,currcol,currslice,:) = sigfun(oetime,oesigbaseline,oeswitch,oeswitchrate,oesigbaseline*1.0);
dcevol(currrow,currcol,currslice,:) = sigfun(dcetime,dcesigbaseline,dceswitch,dceswitchrate,dcesigbaseline*2.5);
% hypoxic
currrow = 3; currcol = 3;
oevol(currrow,currcol,currslice,:) = sigfun(oetime,oesigbaseline,oeswitch,oeswitchrate,oesigbaseline*0.9);
dcevol(currrow,currcol,currslice,:) = sigfun(dcetime,dcesigbaseline,dceswitch,dceswitchrate,dcesigbaseline*2.5);

% necrotic (diffused oxygen present)
currrow = 2; currcol = 4;
oevol(currrow,currcol,currslice,:) = sigfun(oetime,oesigbaseline,oeswitch,oeswitchrate,oesigbaseline*1.05);
dcevol(currrow,currcol,currslice,:) = sigfun(dcetime,dcesigbaseline,dceswitch,dceswitchrate,dcesigbaseline*1.0);
% necrotic
currrow = 3; currcol = 4;
oevol(currrow,currcol,currslice,:) = sigfun(oetime,oesigbaseline,oeswitch,oeswitchrate,oesigbaseline*1.0);
dcevol(currrow,currcol,currslice,:) = sigfun(dcetime,dcesigbaseline,dceswitch,dceswitchrate,dcesigbaseline*1.0);

% duplicate slices
oevol(:,:,3,:) = oevol(:,:,2,:);
dcevol(:,:,3,:) = dcevol(:,:,2,:);
oevol(:,:,4,:) = flipud(oevol(:,:,2,:));
dcevol(:,:,4,:) = flipud(dcevol(:,:,2,:));

% let's create a suitable mask
mask = zeros(spatialsize);
mask(2:3,2:4,2:4) = 1;

% let's also create matrices with the input signals - we should get the
% same result analysing these as analysing the volumes.
% we will turn our volumes into matrices, with time along dim 2, and keep
% only those rows in the mask
oemat = reshape(oevol,prod(spatialsize),oesize(4)); oemat = oemat(mask>0,:);
dcemat = reshape(dcevol,prod(spatialsize),dcesize(4)); dcemat = dcemat(mask>0,:);

%% Test mapping from volume and matrix consistent

% get the hypoxia status as a volume, and as a vector of the nonzero mask elements
[hmap, hstatmat] = hypoxistat(dcevol, [1 6 40 75], oevol, [1 15 50 80], mask);

% let's compare this to a matrix-based hypoxia calculation
hstatmat2 = hypoxistat(dcemat, [1 6 40 75], oemat, [1 15 50 80]);

% and also check whether the mask option is optional
dcevolcropped = reshape(dcevol,prod(spatialsize),dcesize(4)); dcevolcropped = dcevolcropped(mask>0,:); dcevolcropped = reshape(dcevolcropped,2,3,3,dcesize(4));
oevolcropped = reshape(oevol,prod(spatialsize),oesize(4)); oevolcropped = oevolcropped(mask>0,:); oevolcropped = reshape(oevolcropped,2,3,3,oesize(4));
[~, hstatmat3] = hypoxistat(dcevolcropped, [1 6 40 75], oevolcropped, [1 15 50 80]);

% are the analyses the same?
assert(isequal(hmap(mask>0),hstatmat))
assert(isequal(hstatmat, hstatmat2))
assert(isequal(hstatmat, hstatmat3))

% check number with each status correct
assert(nnz(hstatmat==1)==3); % number of necrotic (1) voxels
assert(nnz(hstatmat==2)==3); % number of necrotic (2) voxels
assert(nnz(hstatmat==3)==6); % number of hypoxic voxels
assert(nnz(hstatmat==4)==6); % number of normoxic voxels

% plot hypoxia montage
hypoximapshow(hmap)