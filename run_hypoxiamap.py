import os
import time

import numpy as np
import matplotlib.pyplot as plt

import QbiPy.dce_models.data_io as data_io
from QbiPy.image_io.analyze_format import read_analyze, write_analyze
from QbiPy.tools.runner import QbiRunner, bool_option

from hypoxiamap import hypoxistat, hypoxia_status, hypoxia_interpretation  

#----------------------------------------------
def add_options(runner):
    parser = runner.parser

    #General input argument
    parser.add('--roi', type=str, nargs='?', default=None,
        help='Relative path to roi mask file')

    #OE input options
    parser.add('--oe_dir', type=str, nargs='?', default='oxygen',
        help='Relative path to folder containing OE volumes')
    parser.add('--oe_name', type=str, nargs='?', default='oe_',
        help='Root name of the OE volumes')
    parser.add('--oe_index_fmt', type=str, nargs='?', default='03d',
        help='Index format for the sequential numbering of OE volume names')
    parser.add('--oe_ext', type=str, nargs='?', default='.nii.gz',
        help='File extension of the OE volumes')
    parser.add('--oe_limits', type=int, nargs=4, required=True,
        help='Limits for the OE volumes, 4 element integer list')

    #DCE input options
    parser.add('--dce_dir', type=str, nargs='?', default='dynamic',
        help='Relative path to folder containing DCE volumes')
    parser.add('--dce_name', type=str, nargs='?', default='dyn_',
        help='Root name of the DCE volumes')
    parser.add('--dce_index_fmt', type=str, nargs='?', default='03d',
        help='Index format for the sequential numbering of DCE volume names')
    parser.add('--dce_ext', type=str, nargs='?', default='.nii.gz',
        help='File extension of the DCE volumes')
    parser.add('--dce_limits', type=int, nargs=4, required=True,
        help='Limits for the DCE volumes, 4 element integer list')

    #Hypoxia mapping options
    parser.add('--drift_correction', type=str, nargs='+', default='None',
        help='Type of drift correction to apply')
    parser.add('--drift_roi', type=str, nargs='+', default=None,
        help='Relative path to roi mask file')
    parser.add('--sig_level', type=float, nargs='?', default=0.05,
        help='Relative path to roi mask file')
    
    #Ouput options
    parser.add('--hypoxia_name', type=str, nargs='?', default='hypoxia_map',
        help='Name of the output hypoxia map')
    parser.add('--output_index_fmt', type=str, nargs='?', default='03d',
        help='Index format for the sequential numbering of the output volume names')
    parser.add('--output_ext', type=str, nargs='?', default='.nii.gz',
        help='File extension of the output volumes')    
    parser.add('--output_dce', type=bool_option, nargs='?', default=False, 
        const = False, help='Flag to output the drift corrected DCE volumes')
    parser.add('--output_oe', type=bool_option, nargs='?', default=False, 
        const = False, help='Flag to output the drift correct OE volumes')
    
    parser.add('--make_plots', type=bool_option, nargs='?', default=True, 
        const = True, help='Flag to make output summary plots')
    parser.add('--output_stats_file', type=str, nargs='?', default='hypoxia_stats.csv',
        help='Name of output summary stats file. If empty, stats file not created')

#----------------------------------------------
def run_hypoxiamap(
    data_dir = None, 
    oe_dir = 'oxygen', 
    oe_name = 'oe_', 
    oe_index_fmt = '03d', 
    oe_ext = '.nii.gz',
    dce_dir = 'dynamic', 
    dce_name = 'dyn_', 
    dce_index_fmt = '03d', 
    dce_ext = '.nii.gz',
    roi = None, 
    dce_limits = None, 
    oe_limits = None, 
    drift_correction = 'none',
    drift_roi = None, 
    sig_level = 0.05,
    output_dir = 'hypoxia_status', 
    hypoxia_name = 'hypoxia_map',
    output_ext = '.nii.gz',
    output_dce = False,
    output_oe = False,
    output_stats_file = 'hypoxia_stats',
    make_plots = True):

    #Load in OE and DCE(gd) volumes
    oe_vols, hdrs = read_vols(
        data_dir, oe_dir, oe_name, 
        oe_index_fmt, oe_ext, oe_limits[-1], 'OE')
    dce_vols = read_vols(
        data_dir, dce_dir, dce_name, 
        dce_index_fmt, dce_ext, dce_limits[-1], 'DCE')[0]

    #Load in mask if specified
    if roi is not None:
        roi_path = os.path.join(data_dir, roi)
        roi_mask = read_analyze(roi_path)[0] > 0
        print(f'Loaded ROI mask from {roi_path}')
        total_voxels = np.sum(roi_mask)
    else:
        roi_mask = None
        total_voxels = np.prod(oe_vols.shape[:-1])

    #Load in drift masks if set
    if drift_roi is not None:
        if type(drift_roi) is str:
            drift_roi = [drift_roi]

        if len(drift_roi) == 1:
            drift_roi_path = os.path.join(data_dir, drift_roi[0])
            drift_roi_mask = read_analyze(drift_roi_path)[0] > 0
            drift_roi = [drift_roi_mask, drift_roi_mask]
        elif len(drift_roi) == 2:
            drift_roi_paths = [os.path.join(data_dir, d) for d in drift_roi]
            drift_roi = [read_analyze(d)[0] > 0 for d in drift_roi_paths]

    #Compute hypoxia map
    print(f'Computing hypoxia status for {total_voxels} voxels')
    start = time.time()
    hypoxia_map, dce_corrected, oe_corrected = hypoxistat(
        gd_signals = dce_vols, 
        gd_limits = dce_limits, 
        oe_signals = oe_vols, 
        oe_limits = oe_limits, 
        mask = roi_mask,
        driftCorrectionMethod = drift_correction, 
        drift_mask = drift_roi, 
        siglevel = sig_level)
    end = time.time()
    print(f'Finished in {end - start} seconds')

    #Write output maps
    voxel_size, use_native = get_voxel_size(hdrs[0])

    os.makedirs(output_dir, exist_ok=True)
    hypoxia_path = os.path.join(output_dir, hypoxia_name + output_ext)
    write_analyze(hypoxia_map, hypoxia_path, voxel_size=voxel_size, 
        use_native=use_native, dtype = np.float32)

    #Write drift corrected signals
    if output_dce:
        write_vols(dce_corrected, output_dir, dce_dir + '_corrected_' + drift_correction, 
            dce_name, dce_index_fmt, dce_ext, 
            voxel_size, use_native, 'DCE')

    if output_oe:
        write_vols(oe_corrected, output_dir, oe_dir + '_corrected_' + drift_correction, 
            oe_name, oe_index_fmt, oe_ext, 
            voxel_size, use_native, 'OE')

    #Write output stats
    if output_stats_file is not None:
        #Compute stats
        vox_necrotic = np.sum(hypoxia_map == 1); #necrotic
        vox_diffo2 = np.sum(hypoxia_map == 2); #diffused O2
        vox_hypoxic = np.sum(hypoxia_map == 3); #hypoxic
        vox_normoxic = np.sum(hypoxia_map == 4); #normoxic

        pNEC = 100*vox_necrotic/total_voxels
        pDIFFO2 = 100*vox_diffo2/total_voxels
        pOXYR = 100*vox_hypoxic/total_voxels
        pOXYE = 100*vox_normoxic/total_voxels
        
        #Write to output stats file
        output_stats_path = os.path.join(output_dir, output_stats_file)
        with open(output_stats_path, 'wt') as stats_file:
            print('Hypoxia class:, ' + ', '.join(hypoxia_interpretation), 
                  file = stats_file)
            print(f'Voxels:, {vox_necrotic}, {vox_diffo2}, {vox_hypoxic}, {vox_normoxic}', 
                  file = stats_file)
            print(f'Percentage:, {pNEC}, {pDIFFO2}, {pOXYR}, {pOXYE}', 
                  file = stats_file)

    if make_plots:
        plot_hypoxia_data(
            hypoxia_map, 
            dce_corrected, dce_limits, 
            oe_corrected, oe_limits,
            total_voxels, 
            save_path = os.path.join(output_dir, 'hypoxia_vox_avgs.png'))
        plot_drift_correction(
            dce_vols, dce_corrected, dce_limits, 
            oe_vols, oe_corrected, oe_limits, hypoxia_map > 0,
            drift_correction, 
            save_path = os.path.join(output_dir, 'hypoxia_drift_correction.png'))
#-----------------------------------------------
def read_vols(data_dir, dyn_dir, dyn_name, index_fmt, img_ext, num_vols, vol_type):
    root_path = os.path.join(data_dir, dyn_dir, dyn_name)
    vols, hdrs = data_io.get_dyn_vols(root_path, num_vols, index_fmt, img_ext)

    print(f'Loaded {num_vols} {vol_type} volumes from {root_path}')
    return vols, hdrs

#-----------------------------------------------
def get_voxel_size(hdr):
    try:
        voxel_size = hdr.SformMatrix
        use_native = False
    except:
        voxel_size = hdr.PixelDimensions
        use_native = True
    return voxel_size, use_native

#-----------------------------------------------
def write_vols(vols, data_dir, dyn_dir, dyn_name, index_fmt, img_ext, 
    voxel_size, use_native, vol_type):
    vols_dir = os.path.join(data_dir, dyn_dir)
    root_path = os.path.join(vols_dir, dyn_name)
    os.makedirs(vols_dir, exist_ok=True)
    for i_vol in range(vols.shape[-1]):
        vol_path = f"{root_path}{i_vol+1:{index_fmt}}{img_ext}"
        write_analyze(
            vols[:,:,:,i_vol], vol_path, voxel_size=voxel_size, 
            use_native=use_native, dtype = np.float32)

    print(f'Saved drift corrected {vol_type} volumes to {root_path}')

#-----------------------------------------------
def plot_hypoxia_data(
    hypoxia_map, dce_corrected, dce_limits, oe_corrected, oe_limits,
    total_voxels, save_path = ''):
    
    fig, ax = plt.subplots(2, 4, layout = 'constrained', figsize=(20,10))
    
    oe_vox_averages = np.zeros((oe_corrected.shape[3], 4))
    dce_vox_averages = np.zeros((dce_corrected.shape[3], 4))

    for h_index in [1,2,3,4]:
        h_map = hypoxia_map == h_index
        num_vox = np.sum(h_map)
        pct_vox = 100*num_vox / total_voxels
        
        h_interp = hypoxia_interpretation[h_index - 1]  
        h_status = hypoxia_status[h_index - 1]   
        title_info = (f'{h_index}: {h_interp} \n ({h_status})\n' 
            f'{num_vox:,} voxels, {pct_vox:2.1f}%')
        plot_col = (h_index-1)
        if h_index == 1:
            oe_label = 'OE'
            dce_label = 'DCE'
        else:
            oe_label = ''
            dce_label = ''

        oe_vox_averages[:,plot_col] = plot_avg_voxels(
            ax[0,plot_col], 
            h_map, oe_corrected, oe_limits, title_info, oe_label,
            legend_on = h_index==1)

        dce_vox_averages[:,plot_col] = plot_avg_voxels(
            ax[1,plot_col], 
            h_map, dce_corrected, dce_limits, '', dce_label)

        fig.supxlabel('Timepoint')

    #Fix limits
    oe_ylims = compute_ylimits(oe_vox_averages)
    dce_ylims = compute_ylimits(dce_vox_averages)
    for plot_col in range(4):
        plot_baseline_enhancing_limits(ax[0,plot_col], oe_ylims[:,plot_col], oe_limits)
        plot_baseline_enhancing_limits(ax[1,plot_col], dce_ylims[:,plot_col], dce_limits)

    if save_path:
        fig.savefig(save_path, bbox_inches='tight', facecolor='w')

        csv_path = os.path.splitext(save_path)[0]
        np.savetxt(csv_path + "_oe.csv", oe_vox_averages, 
                   delimiter=',', fmt = '%10.5f',
                   header = ','.join(hypoxia_interpretation))
        np.savetxt(csv_path + "_dce.csv", dce_vox_averages, 
                   delimiter=',', fmt = '%10.5f',
                   header = ','.join(hypoxia_interpretation))

#-----------------------------------------------
def compute_ylimits(vox_averages):
    #Compute limits range as 5% bigger than maximum column range
    col_mins = np.min(vox_averages, axis = 0)
    max_range = 1.05*np.max(np.max(vox_averages, axis=0) - col_mins)

    #Set each y-limit so the minimum value of the series is at 2.5% of the
    #range
    y_limits = np.zeros((2,4))
    y_limits[0,:] = col_mins - 0.025*max_range
    y_limits[1,:] = y_limits[0,:] + max_range

    return y_limits
#-----------------------------------------------
def plot_baseline_enhancing_limits(ax, ylims, limits):
    limit_colors = ('g', 'r')
    ax.set_ylim(ylims)
    ax.fill_between(limits[:2], ylims[0], ylims[1], 
        color=limit_colors[0], alpha = 0.1)
    ax.fill_between(limits[2:], ylims[0], ylims[1], 
        color=limit_colors[1], alpha = 0.1)

#-----------------------------------------------
def plot_avg_voxels(ax, h_map, dyn_vols, limits, title_info, dyn_type, 
    legend_on = False):

    if np.sum(h_map):
        vox_avg = np.mean(dyn_vols[h_map], axis = 0)
    else:
        vox_avg = np.zeros((dyn_vols.shape[3]))

    timepoints = np.arange(1, vox_avg.shape[0] + 1)
    y_base = np.percentile(vox_avg[limits[0]-1:limits[1]], [5,95])
    y_enh = np.percentile(vox_avg[limits[2]-1:limits[3]], [5,95])

    vox_color = 'k'
    vox_marker = 'o'
    limit_colors = ('g', 'r')

    ax.plot(timepoints, vox_avg, linewidth = 2.0, 
        marker = vox_marker, color = vox_color,
        label = f'Mean signal')

    ax.fill_between(limits[:2], y_base[0], y_base[1], 
        color=limit_colors[0], alpha = 0.4,
        label = f'Baseline period')
    ax.fill_between(limits[2:], y_enh[0], y_enh[1], 
        color=limit_colors[1], alpha = 0.4,
        label = f'Enhancing period')
    
    ax.set_ylabel(dyn_type, rotation = 0, fontsize=20, labelpad = 40)
    ax.set_title(title_info, fontsize = 18)

    if legend_on:
        ax.legend()

    return vox_avg
        
    
#-----------------------------------------------
def plot_drift_correction(
    dce, dce_corrected, dce_limits, oe, oe_corrected, oe_limits, roi_mask,
    drift_correction, save_path = ''):  
    
    fig, ax = plt.subplots(1, 2, layout = 'constrained', figsize=(10,5))

    dce_avg = plot_drift(ax[0], dce, dce_corrected, dce_limits, roi_mask, 'DCE')
    oe_avg = plot_drift(ax[1], oe, oe_corrected, oe_limits, roi_mask, 'OE')
    fig.suptitle(f'Drift correction applied = {drift_correction}')
    fig.supxlabel('Timepoint')
    fig.supylabel('Mean signal')

    if save_path:
        fig.savefig(save_path, bbox_inches='tight', facecolor='w')

        csv_path = os.path.splitext(save_path)[0]
        np.savetxt(csv_path + "_oe.csv", oe_avg, 
                   delimiter=',', fmt = '%10.5f',
                   header = 'Original, Corrected')
        np.savetxt(csv_path + "_dce.csv", dce_avg, 
                   delimiter=',', fmt = '%10.5f',
                   header = 'Original, Corrected')

#-----------------------------------------------
def plot_drift(ax, dyn, dyn_corrected, limits, roi_mask, dyn_type):

    ax.set_title(dyn_type)
    dyn_avg = plot_drift_series(ax, dyn, limits, roi_mask, 'b', 'Original')
    dyn_corrected_avg = plot_drift_series(
        ax, dyn_corrected, limits, roi_mask, 'r', 'Corrected')
    ylims = ax.get_ylim()
    ax.fill_between(limits[:2], ylims[0], ylims[1], 
        color='g', alpha = 0.1, label = 'Baseline period')
    if dyn_type == 'DCE':
        ax.legend()

    return np.array((dyn_avg, dyn_corrected_avg)).transpose()

#-----------------------------------------------
def plot_drift_series(ax, dyn_vols, limits, roi_mask, color, label):
    dyn_avg = np.mean(dyn_vols[roi_mask], axis = 0)
    timepoints = np.arange(1, dyn_avg.shape[0] + 1)

    x_baseline = np.arange(limits[0]-1, limits[1])
    dyn_baseline = dyn_avg[limits[0]-1:limits[1]]

    P = np.polyfit(x_baseline, dyn_baseline, 1)
    dyn_baseline_avg = np.mean(dyn_baseline)

    x_fit = timepoints[[0,-1]]
    y_fit = P[0]*x_fit + P[1]

    ax.plot(timepoints, dyn_avg, color = color, linestyle = '--', marker = 'o',
        linewidth = 1.0, label = f'{label}, voxel avg.')
    ax.plot(x_fit, y_fit, color = color, linestyle = '-',
        linewidth = 3.0, label = f'{label}, baseline projection')

    if label == 'Original':
        ax.plot(x_fit, [dyn_baseline_avg, dyn_baseline_avg], 
            'k--', label = 'Flat baseline')
        
    return dyn_avg

#----------------------------------------------
def main():
    runner = QbiRunner()
    add_options(runner)
    runner.run(run_hypoxiamap)
#----------------------------------------------
if __name__ == "__main__":
    main()